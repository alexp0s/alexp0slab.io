---
title: "Fiddling With Themes"
authors: ["alexpos"]
date: 2019-07-20
tags: [hugo, podcasts]
draft: false
---

# (Further) theming Hugo

Hugo comes with a large amount of [themes](https://themes.gohugo.io/). Finding [a theme I liked](https://themes.gohugo.io/er/) was pretty easy, but within a day I was already seeing things I wanted to change. These are some notes I took while fiddling with my theme.

## Installing a theme as a submodule ##

To install a theme as a submodule (so you can easily update it), use the following comands:

	cd your-hugo-site
	mkdir -p themes
	git submodule add https://github.com/lingxz/er themes/er

To update:

	git submodule foreach git pull

## Adding drafts ##

The easiest way to make a theme your own is to add a *layout* folder to your Hugo root-folder, copy the file you want to change, and make your changes there. This gives you the possibility to revert your changes easily (just remove the file), but also keep the theme updated (if you installed it as a submodule).

    {{ if .Draft }} (draft){{end}}

# Todays podcasts

* [The Best Editors and IDEs for Teaching Python - Teaching Python](https://pca.st/ss50)

	Especially liked the discussion of the [Mu editor](https://codewith.mu/).
* [Chat with the authors of the book "The Fifth Domain" - Open Source Security Podcast](https://www.opensourcesecuritypodcast.com/2019/07/episode-154-chat-with-authors-of-book.html)

	This book, [The Fifth Domain: Defending Our Country, Our Companies, and Ourselves in the Age of Cyber Threats: Richard A. Clarke, Robert K. Knake: 9780525561965: Amazon.com: Books](https://www.amazon.com/Fifth-Domain-Defending-Companies-Ourselves/dp/052556196X) is on my wish-list. Cyberwarfare from a military perspective: how do we protect our infrastructure, and who do we prosecute when things go wrong.

* [Linux 5.2, Debian 10, AMD, System76’s Thelio, Valve’s Steam Labs & more – Destination Linux EP130 - Destination Linux](https://pca.st/F586)
  * [Steam Labs](https://store.steampowered.com/labs)
  * [Intent to provide chromium as a snap only - Desktop - Ubuntu Community Hub](https://discourse.ubuntu.com/t/intent-to-provide-chromium-as-a-snap-only/5987)

* [A Decision in the Eric Garner Case - The Daily](https://pca.st/2c2F)

	One day before the fifth anniversary of Eric Garner’s death at the hands of police officers in New York, the Justice Department said it would not bring federal civil rights charges against an officer involved. 

* ["If She Would Like to Rephrase That Comment" - Start Here](https://pca.st/566K)
	
	Congress erupts over whether President Trump's tweets were racist. The Justice Department decides not to bring charges over Eric Garner's death. And Puerto Rico braces for protests tonight. 
