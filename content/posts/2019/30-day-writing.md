---
title: "30 day writing challenge"
date: 2019-07-17T02:01:58+05:30
authors: ["alexpos"]
description: "Challenging oneself"
tags: [writing, podcasts]
---

## Step 1

A friend of mine mentioned that he did a 30 day challenge. The idea is that you do something for a reasonably limited amount of time, and this will help you to achieve goals and create new habits. It sounded a bit hokey to me, but after thinking about it I decided to give it a try. My 30 day challenge is *writing a daily blog post for thirty days*.

Now, nobody has time to do something like that, but on the other hand, it's only thirty days, right?

## GitLab pages

So, I created a new site on GitLab, read [how to use GitLab pages](https://about.gitlab.com/2016/04/07/gitlab-pages-setup/), and forked an org-mode template. Next, before I know it, I spend two hours trying to get org-mode publish to do what I want, writing elisp snippets to create a descent list of posts, etc. The idea was writing text, not reading [Stack Overflow](https://stackoverflow.com/), so away with org-mode, and here comes Hugo!

On my workstation I have installed [GitKraken](https://www.gitkraken.com/git-client), a nice git front-end, and [UberWriter](https://uberwriter.github.io/uberwriter/#1), a markdown editor. All this is also possible from the cli or Emacs, but it is a nice change of pace.

## Podcasts

Today I listened to several podcasts:

* [How Some Florida Prosecutors Are Pushing Back Against GOP Voter Suppression Efforts - Fearless, Adversarial Journalism – Spoken Edition](https://pca.st/4zn1)
* [How Israeli-Designed Drones Became Russia’s Eyes in the Sky for Defending Bashar al-Assad - Fearless, Adversarial Journalism – Spoken Edition](https://pca.st/64Gz)
* [Trump and ‘the Squad’ - The Daily](https://pca.st/4n28)
* [#SquadGoals - Start Here](https://pca.st/2sh0)
* [Encrypting DNS - Security Now (MP3)](https://pca.st/7VJ8)

Security now had some good parts, but the show is too long, I always end up skipping. I should really find a replacement.